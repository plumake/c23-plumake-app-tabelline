import Home from './App';
import {name as appName} from './app.json';
import { StyleSheet, Image, AppRegistry, StatusBar } from 'react-native';
import { Header } from '@rneui/base';
import { SafeAreaProvider } from 'react-native-safe-area-context';


const styles = StyleSheet.create({
	imageLogo: {
		width: '100%',
		height: 60,
	},
	headerContainer: {
		padding: 0,
		backgroundColor: '#ffffff',
		width: '100%',
		borderBottomRightRadius: 30,
		borderBottomLeftRadius: 30,
	},
});

function App() {
  return(
    <SafeAreaProvider style={{backgroundColor: '#414a4c'}}>
      <Header
        containerStyle={styles.headerContainer}
        centerComponent={<Image resizeMode='contain' source={require('./assets/plumake_logo.png')} style={styles.imageLogo} />}
      />
			<StatusBar
				backgroundColor="#933089" 
			/>
    	<Home/>
    </SafeAreaProvider>
    );
}


AppRegistry.registerComponent(appName, () => App);


