import Tts from 'react-native-tts';
import Voice, { SpeechResultsEvent } from '@react-native-voice/voice';
import * as iWtoN from 'italian-words-to-numbers';
import React, { useState, useEffect, useRef } from 'react';
import { FlatList, Text, TouchableOpacity, StyleSheet, View } from 'react-native';
import { Icon } from '@rneui/themed';

const tabelline_list = [2,3,4,5,6,7,8,9,10];

export default function Home() {
  const [tabelline, setTabelline] = useState<number[]>([]);
  const [bottoni, setBottoni] = useState<boolean[]>([false, false, false]);
  const [quiz, setQuiz] = useState<string>("0 x 0");
  const listen = useRef<boolean>(false);
  const tab = useRef<number>(0);
  const mul = useRef<number>(0);
  const play = useRef<boolean>(false);

  //executed on first render
  useEffect(() => {
    Voice.onSpeechStart = onSpeechStart;
    Voice.onSpeechResults = onSpeechResults;
    Voice.onSpeechEnd = onSpeechEnd;
    Tts.addEventListener('tts-finish', () => {
      console.log("finish talk");
      if(listen.current && play.current){
        Voice.start('IT-IT',);
        listen.current = false
      }
    });
    
  }, []);

  function onSpeechEnd() {
    console.log("end listen");
  }

  function onSpeechStart(){
    console.log("start listen");
  }

  function onSpeechResults(params:SpeechResultsEvent){
    console.log("onSpeechResults: ", params);
    var n = 0;
    try {
      n = iWtoN.convert(params.value![0])
    } catch (error) {
      Tts.speak(params.value![0] + " non è un numero");
      return;
    }
    if(n === tab.current * mul.current){
      Tts.speak("Bravo");
    }else{
      Tts.speak("Sbagliato");
    }
    if(play.current){
      startQuiz();
    }
  }

  //start the quiz by selecting a random tabellina and a random multiplier
  function startQuiz(){
    if(tabelline.length === 0){
      play.current = false;
      return;
    }
    listen.current = true;
    tab.current = tabelline[Math.floor(Math.random() * tabelline.length)];
    mul.current = Math.floor(Math.random() * 10) + 1
    const quiz = tab.current + " x " + mul.current;
    setQuiz(quiz);
    console.log(quiz);
    Tts.speak(quiz);
  }

  //add or remove a tabellina from the list
  function AR_tabellina (num:number) : void {
    for(let i = 0; i < tabelline.length; i++){
      if(tabelline[i] === num){
        bottoni[num-2] = false;
        tabelline.splice(i, 1);
        setBottoni([...bottoni]);
        setTabelline([...tabelline]);
        return;
      }
    }
    bottoni[num-2] = true;
    tabelline.push(num);
    setBottoni([...bottoni]);
    setTabelline([...tabelline]);
  }

  return (
      <>
        <View>
          <FlatList
            data={tabelline_list}
            renderItem={({item}) => (<TouchableOpacity style={[styles.tab_button, {backgroundColor: bottoni[item-2] ? '#99999a' : '#933089'}]} onPress={() => AR_tabellina(item)} ><Text style={styles.text_button}>{item.toString()}</Text></TouchableOpacity>)}
            numColumns={4}
            style={{marginBottom: 10}}
          />
        </View>
        <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize: 50, color: 'white'}}>{quiz}</Text>
        </View>
        <View style={styles.bottomNav}>
            <TouchableOpacity style={styles.buttonNav} onPress={() => {
              if(!play.current){
                play.current = true;
                startQuiz()
              }
            }}>
              <Icon size={20} style={{margin: 10}} name="play" type='font-awesome' color={'green'}/>
              <Text>Inizia</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonNav} onPress={() => play.current = false}>
              <Icon size={20} style={{margin: 10}} name="stop" type='font-awesome' color={'red'}/>
              <Text>Stop</Text>
            </TouchableOpacity>
        </View>
      </>
  );
}

const styles = StyleSheet.create({
  imageLogo: {
    width: '100%',
    height: 50,
  },
  headerContainer: {
    padding: 0,
    backgroundColor: '#ffffff',
    width: '100%',
    borderBottomRightRadius: 30,
    borderBottomLeftRadius: 30,
  },
  tab_button: {
    flex: 1,
    margin: 7,
    padding: 10,
    borderRadius: 10,
  },
  text_button: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
  },
  bottomNav: {
    justifyContent: 'space-evenly',
    width: '100%',
    height: '14%',
    flexDirection: 'row',
    backgroundColor: 'white',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
  },
  buttonNav: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height: 'auto',
    width: '22%',
    aspectRatio: 1,
    backgroundColor: '#E9E9E9',
    margin: 10,
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 2, // Android
  }
});